import { ApiProperty } from '@nestjs/swagger';
import { IsString } from 'class-validator';
/**
 * @ignore
 */
export class WSMessageBodyDTO {
  @ApiProperty()
  @IsString()
  authorization: string;

  @ApiProperty()
  @IsString()
  data: any;
}
