import { EventsModule } from '../websockets/websockets.module';
import {
  //CacheModule,
  Module,
  //CacheInterceptor,
  OnModuleInit,
  HttpException,
} from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import * as Joi from '@hapi/joi';

//controller
import { AppController } from './app.controller';

//entities
import { UserEntity } from '../modules/users/model/user.entity';
import { ProfileEntity } from '../modules/profiles/model/profile.entity';
import { RuleEntity } from '../modules/rules/model/rule.entity';

//modules
import { AuthModule } from '../auth/auth.module';
import { ProfilesModule } from '../modules/profiles/profiles.module';
import { RulesModule } from '../modules/rules/rules.module';
import { UsersModule } from '../modules/users/users.module';
import { DefinitionsModule } from '../modules/definitions/definitions.module';

//services
import { AppService } from './app.service';
import { MyLogger } from '../common/services/logger.service';
import { SyslogModule } from '../modules/syslog/syslog.module';
import { SyslogEntity } from '../modules/syslog/model/syslog.entity';
import { SyslogService } from '../modules/syslog/syslog.service';
import { SyslogInterceptor } from '../modules/syslog/syslog.interceptor';
import { SentryInterceptor } from '../sentry/sentry.interceptor';
import { MateriasModule } from '../modules/materias/materias.module';
import { ConfigKeys } from '../common/enum/configkeys.enum';
/**
 * Módulo principal de la app
 */
@Module({
  imports: [
    //cargar configuracion de .env
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        NODE_ENV: Joi.string()
          .valid('development', 'production', 'test', 'provision')
          .default('development'),
        PORT: Joi.number().default(3000),
        API_ROUTE: Joi.string().default('api/v1'),
        API_SWAGGER: Joi.string().default('swagger'), // = /api/v1/swagger
        //jwt
        JWT_SECRET: Joi.string().required(),
        JWT_EXPIRATION: Joi.string().default('4h'),

        //usuarios
        CREATE_USERS: Joi.boolean().default(true),
        FIRST_PASSWORD: Joi.string().default('password'),

        //mysql
        MYSQL_DB: Joi.string().required(),
        MYSQL_HOST: Joi.string().required(),
        MYSQL_USER: Joi.string().required(),
        MYSQL_PORT: Joi.number().default(3306),
        MYSQL_PASSWORD: Joi.string().required(),
        MYSQL_ROOT_PASSWORD: Joi.string().default('nada'),
        //redis
        REDIS_HOST: Joi.string().required(),
        REDIS_AUTH: Joi.string().required(),
        REDIS_PORT: Joi.number().default(6379),
        REDIS_DB: Joi.number()
          .valid(0, 1, 2, 3, 4, 5, 6)
          .default(0),
        MFA_ENABLED: Joi.string().default(''),
        TWO_FACTOR_AUTHENTICATION_APP_NAME: Joi.string().default(
          'Nest Plataforma Base',
        ),
        //sentry.io
        SENTRY_DSN: Joi.string().default(''),
      }),
      validationOptions: {
        allowUnknown: true, //permitir valores no definidos en validationSchema dentro del .env
        abortEarly: true, //abortar al primer error encontrado
      },
      isGlobal: true, // permitirlo de manera globaL
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (_configService: ConfigService) => ({
        //conectar a la bd
        type: 'mysql',
        host: _configService.get<string>(ConfigKeys.MYSQL_HOST),
        port: parseInt(_configService.get<string>(ConfigKeys.MYSQL_PORT)),
        username: _configService.get<string>(ConfigKeys.MYSQL_USER),
        password: _configService.get<string>(ConfigKeys.MYSQL_PASSWORD),
        database: _configService.get<string>(ConfigKeys.MYSQL_DB),
        entities: [
          //entidades en esa base de datos
          UserEntity,
          ProfileEntity,
          RuleEntity,
          SyslogEntity,
        ],
        synchronize: true, //generación  y sincronización de las tablas segun el entity
      }),
      inject: [ConfigService],
    }) /*
    TypeOrmModule.forRoot(),*/,
    //Módulos a inicializar al arranque
    AuthModule,
    RulesModule,
    UsersModule,
    ProfilesModule,
    EventsModule,
    DefinitionsModule,
    SyslogModule,
    MateriasModule,
  ],
  controllers: [AppController],
  providers: [
    //sentry.interceptor
    {
      provide: APP_INTERCEPTOR,
      useValue: new SentryInterceptor({
        filters: [
          // filtrar exceptions de tipo HttpException. ignorar aquellas
          // con status code menor a 500
          {
            type: HttpException,
            filter: (exception: HttpException): boolean =>
              500 > exception.getStatus(),
          },
        ],
      }),
    },
    {
      //interceptor de log de sistema
      provide: APP_INTERCEPTOR,
      useClass: SyslogInterceptor,
    },
    MyLogger,
    AppService,
    SyslogService,
    ConfigService,
  ],
})
export class AppModule implements OnModuleInit {
  constructor(
    private readonly _logger: MyLogger,
    private readonly _app: AppService,
  ) {}
  async onModuleInit(): Promise<boolean> {
    //llamar al servicio de inicialización de la base de datos
    await this._app.initDatabase();
    //la aplicación ha iniciado
    this._logger.log('App initialized.');
    return true;
  }
}
