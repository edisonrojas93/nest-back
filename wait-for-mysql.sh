#!/bin/bash

#funcion para obtener valores de .env
read_var() {
  if [ -z "$1" ]; then
    echo "environment variable name is required"
    return
  fi

  local ENV_FILE='.env'
  if [ ! -z "$2" ]; then
    ENV_FILE="$2"
  fi

  local VAR
  VAR=$(grep $1 "$ENV_FILE" | xargs)
  IFS="=" read -ra VAR <<< "$VAR"
  echo ${VAR[1]}
}

#obtener valores de .env
MYSQL_PORT=$(read_var MYSQL_PORT .env)
MYSQL_HOST=$(read_var MYSQL_HOST .env)

echo "Waiting for mysql to start..."

until nc -z $MYSQL_HOST $MYSQL_PORT
do
  echo "Waiting for mysql to start..."
  sleep 3
done
echo "Mysql started."