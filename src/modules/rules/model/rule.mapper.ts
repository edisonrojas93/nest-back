import { RuleDTO } from './rule.dto';
import { RuleEntity } from './rule.entity';
/**
 * @ignore
 */
export class RuleMapper {
  dtoToEntity(ruleDTO: RuleDTO): RuleEntity {
    return new RuleEntity(
      ruleDTO.id,
      ruleDTO.name,
      ruleDTO.description,
      ruleDTO.value,
    );
  }

  entityToDto(ruleEntity: RuleEntity): RuleDTO {
    return new RuleDTO(
      ruleEntity.id,
      ruleEntity.name,
      ruleEntity.description,
      ruleEntity.value,
    );
  }
}
