import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { UserDTO } from './model/user.dto';
import { UserEntity } from './model/user.entity';
import { UserMapper } from './model/user.mapper';
/**
 * Repositorio de usuarios
 */
@Injectable()
export class UsersRepository {
  constructor(
    @InjectRepository(UserEntity)
    private usersRepository: Repository<UserEntity>,
    private mapper: UserMapper,
  ) {}

  getAllUsers(): Promise<UserEntity[]> {
    return this.usersRepository.find({
      relations: ['profile', 'profile.rules'],
    });
  }

  getUserById(id: number): Promise<UserEntity> {
    return this.usersRepository.findOne(id, {
      relations: ['profile', 'profile.rules'],
    });
  }

  getUserByUsername(username: string): Promise<UserEntity> {
    return this.usersRepository.findOne(
      { username: username },
      { relations: ['profile', 'profile.rules'] },
    );
  }

  async newUser(userDTO: UserDTO): Promise<UserEntity> {
    const newUser = this.mapper.dtoToEntity(userDTO);
    return this.usersRepository.save(newUser);
  }

  async updateUser(id: number, userDTO: UserDTO): Promise<UserEntity> {
    const updateUserDTO: UserDTO = new UserDTO(
      id,
      userDTO.uuid,
      userDTO.username,
      userDTO.email,
      userDTO.firstName,
      userDTO.lastName,
      userDTO.password,
      userDTO.active,
      userDTO.rules,
      userDTO.profile,
    );
    const updateUser = this.mapper.dtoToEntity(updateUserDTO);
    await this.usersRepository.update(id, updateUser);
    return this.usersRepository.findOne(id);
  }

  deleteUser(id: number): Promise<DeleteResult> {
    return this.usersRepository.delete(id);
  }
}
