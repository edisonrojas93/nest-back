/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  Controller,
  Get,
  Post,
  UseGuards,
  Param,
  Delete,
  Put,
  ParseIntPipe,
  Body,
  Header,
  UseInterceptors,
  UploadedFile,
  Request,
  Headers,
  Response,
  CacheKey,
} from '@nestjs/common';
import { MyLogger } from '../../common/services/logger.service';

import { Rules } from '../../common/decorators/rules.decorator';
import { RulesGuard } from '../../auth/guards/rules/rules.guard';
import { JwtAuthGuard } from '../../auth/guards/jwt/jwt-auth.guard';

import { UserDTO } from './model/user.dto';
import { UsersService } from './users.service';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
import { FileInterceptor } from '@nestjs/platform-express';
import { renameSync } from 'fs';
/**
 * Controlador de usuarios
 */
@ApiBearerAuth()
@ApiTags('users')
@Controller('users') // /users
@UseGuards(JwtAuthGuard, RulesGuard) //usar estos guards en todo el controller
export class UsersController {
  constructor(private usersService: UsersService, private logger: MyLogger) {}

  // intento de subir archivo!
  @Post('upload-avatar') // /users/upload-picture
  @Header('mi-header', 'mi-valor') //headers en la respuesta
  @UseInterceptors(
    FileInterceptor('avatar', {
      limits: {
        fileSize: 1024 * 1024 * 3, //tamaño de archivo hasta 3MB
      },
      fileFilter: (req, file, cb) => {
        //solo aceptar jpegs
        if (file.mimetype === 'image/jpeg') {
          return cb(null, true);
        }
        return cb(
          new Error(
            'Tipo de archivo no aceptado, se aceptan solamente "image/jpeg".',
          ),
          false,
        );
      },
      dest: './upload/user',
    }),
  )
  uploadFile(@UploadedFile() file, @Headers() headers, @Request() req): any {
    const currentUser: UserDTO = req.user;
    const fileNameDest = currentUser.uuid + '.jpg';
    //cambiar nombre del archivo por el uuid del usuario
    renameSync(
      './upload/user/' + file.filename,
      './upload/user/' + fileNameDest,
    );
    return file;
  }

  @Get('avatar') //users/avatar
  @CacheKey('user_avatar')
  seeUploadedFile(@Request() req, @Response() res) {
    const image = `${req.user.uuid}.jpg`;
    return res.sendFile(image, { root: './upload/user' });
  }

  @Get('picture/:id') //   /users/picture/:id
  @Rules('users_view') //establecer las rules a usar en este metodo
  async getUsuarioPicture(
    @Param('uuid') uuid: string,
    @Response() res,
  ): Promise<any> {
    const image = `${uuid}.jpg`;
    return res.sendFile(image, { root: './upload/user' });
  }

  @Get(':id') // /users/usuarios
  @Rules('users_view') //establecer las rules a usar en este metodo
  async getUsuario(@Param('id', ParseIntPipe) id: number): Promise<UserDTO> {
    return await this.usersService.getUserById(id);
  }
  @Put(':id')
  @Rules('users_update') //establecer las rules a usar en este metodo
  async updateUser(
    @Param('id') id: number,
    @Body() user: UserDTO,
  ): Promise<UserDTO> {
    return await this.usersService.updateUser(id, user);
  }

  @Delete(':id')
  @Rules('users_delete') //establecer las rules a usar en este metodo
  async deleteUser(@Param('id') id: number): Promise<void> {
    return await this.usersService.deleteUser(id);
  }

  @Get() // /users
  @Rules('users_view') //establecer las rules a usar en este metodo
  async getAllUsers(): Promise<UserDTO[]> {
    return await this.usersService.getAllUsers();
  }

  @Post()
  @Rules('users_create') //establecer las rules a usar en este metodo
  async newUser(@Body() user: UserDTO): Promise<UserDTO> {
    return await this.usersService.newUser(user);
  }
}
