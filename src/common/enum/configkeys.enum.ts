/**
 * @ignore
 */
export enum ConfigKeys {
  JWT_SECRET = 'JWT_SECRET',
  JWT_EXPIRATION = 'JWT_EXPIRATION',
  TWO_FACTOR_AUTHENTICATION_APP_NAME = 'TWO_FACTOR_AUTHENTICATION_APP_NAME',
  SENTRY_DSN = 'SENTRY_DSN',
  API_ROUTE = 'API_ROUTE',
  CREATE_USERS = 'CREATE_USERS',
  FIRST_PASSWORD = 'FIRST_PASSWORD',
  MYSQL_DB = 'MYSQL_DB',
  MYSQL_HOST = 'MYSQL_HOST',
  MYSQL_USER = 'MYSQL_USER',
  MYSQL_PORT = 'MYSQL_PORT',
  MYSQL_PASSWORD = 'MYSQL_PASSWORD',
  MYSQL_ROOT_PASSWORD = 'MYSQL_ROOT_PASSWORD',
  REDIS_HOST = 'REDIS_HOST',
  REDIS_AUTH = 'REDIS_AUTH',
  REDIS_PORT = 'REDIS_PORT',
  REDIS_DB = 'REDIS_DB',
  API_SWAGGER = 'API_SWAGGER',
  MFA_ENABLED = 'MFA_ENABLED',
  PORT = 'PORT',
}
