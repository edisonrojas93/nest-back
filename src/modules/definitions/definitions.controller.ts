import { Controller, Post, Body } from '@nestjs/common';
import { CreateDefDTO } from './dto/create-def.dto';
import { DefinitionsService } from './definitions.service';
import { Definition } from './interfaces/definition.interface';
import { ApiTags, ApiBearerAuth } from '@nestjs/swagger';
/**
 * Controller para definiciones
 */
@ApiBearerAuth()
@ApiTags('Definitions')
@Controller('definitions')
export class DefinitionsController {
  constructor(private definitionsService: DefinitionsService) {}

  @Post()
  newDefinition(@Body() createDefDto: CreateDefDTO): Promise<Definition> {
    return this.definitionsService.create(createDefDto);
  }
}
