import { ApiProperty } from '@nestjs/swagger';
import { IsUUID, IsNumber, IsString } from 'class-validator';
/**
 * @ignore
 */
export class JWTPayLoadDTO {
  @ApiProperty()
  @IsString()
  username: string;
  @ApiProperty()
  @IsUUID()
  uuid: string;
  @ApiProperty()
  @IsNumber()
  sub: number;
  @ApiProperty()
  rules: string[];
  @ApiProperty()
  profile: string;
  @ApiProperty()
  mfaRequired: boolean;
  @ApiProperty()
  mfaPassed: boolean;
}
