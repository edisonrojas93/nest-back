import { ProfileEntity } from '../profiles/model/profile.entity';
import { UserDTO } from './model/user.dto';
/**
 * @ignore
 */
const perfilSuper = new ProfileEntity(1, 'super');
//WARNING!; esto suponiendo que los perfiles se crearon con la definición de profiles
/**
 * @ignore
 */
const perfilAdmin = new ProfileEntity(2, 'admin');
//WARNING!; esto suponiendo que los perfiles se crearon con la definición de profiles
/**
 * Usuarios en sistema, el password se crea dentro de app.service
 * con el default de .env
 */
export const users: UserDTO[] = [
  {
    username: 'super',
    firstName: 'Super',
    lastName: 'Admin',
    email: 'super@dominio.com',
    profile: perfilSuper,
    password: '',
  },
  {
    username: 'admin',
    firstName: 'Administrador',
    lastName: 'de Sistema',
    email: 'admin@dominio.com',
    profile: perfilAdmin,
    password: '',
  },
];
