/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
/**
 * Controller principal de la aplicacion
 */
@ApiTags('main')
@Controller() // route = /
export class AppController {
  @Get()
  async getHome(): Promise<string> {
    return 'Hello World!';
  }
}
